#include <iostream>

using namespace std;

void exadecimal (int num)
{
	int result;

	if (num <= 9){
		cout << num;
		return;
	}
	result = num % 16;
	exadecimal (num / 16);

	if (result == 10)
	{
		  cout << 'A';
	}
	  else if (result == 11)
	  {
		  cout << 'B';
	  }
	  else if (result == 12)
	  {
		  cout << 'C';
	  }
	  else if (result == 13)
	  {
		  cout << 'D';
	  }
	  else if (result == 14){
		  cout << 'E';
	  }
	  else if (result == 15)
	  {
		  cout << 'F';
	  }
	  else {
		  cout << result;
	  }
}

int main() 
{
	int dec;
	cout << "INserisci il Decimale da convertire : ";
	cin >> dec;

	if (dec < 0)
		cout << dec << "Non è un numero intero." << endl;
	else {
		cout << "Il numero convertito in Esadecimale: ";
		exadecimal (dec);
   }
   return 0;
}
